package api;

import org.pf4j.ExtensionPoint;

        import java.util.List;

public interface Extraction extends ExtensionPoint {

    List<String> getData();

}
